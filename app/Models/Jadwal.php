<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $fillable = ['tahun_id', 'kelas_id', 'hari_id', 'mapel_id', 'user_id', 'jam_mulai', 'jam_akhir'];

    public function mapel()
    {
        return $this->belongsTo(Mapel::class);
    }

    public function hari()
    {
        return $this->belongsTo(Hari::class);
    }

    public function kelas()
    {
        return $this->belongsTo(Kelas::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
