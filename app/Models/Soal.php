<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $dates = ['start', 'expired'];
    protected $fillable = ['user_id', 'materi_id', 'nama_soal', 'start', 'expired'];

    public function user_s(){
        return $this->belongsToMany('App\Models\User', 'hasil_soal')->withTimeStamps()->withPivot(['id', 'user_id', 'soal_id', 'nilai', 'status']);
    }

    // relasi many to many (feedback dengan segment)
    public function segments(){
        return $this->belongsToMany('App\Models\Segment', 'soal_segment')->withTimeStamps()->withPivot(['id'],['soal_id'], ['segment_id']);
    }
}
