<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    protected $fillable = ['user_id', 'mapel_id', 'nama_segment'];

    // relasi many to many (segment dengan quiz)
    public function soals(){
        return $this->belongsToMany('App\Models\Soal', 'soal_segment')->withTimeStamps()->withPivot(['id'],['soal_id'], ['segment_id']);
    }

    // relasi many to many (segment dengan quiz)
    public function pertanyaans(){
        return $this->belongsToMany('App\Models\Pertanyaan', 'pertanyaan_segment')->withTimeStamps()->withPivot(['id'],['segment_id'], ['pertanyaan_id']);
    }
}
