<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JawabSoal extends Model
{
    protected $fillable = ['user_id', 'soal_id', 'pertanyaan_id', 'jawaban'];
}
