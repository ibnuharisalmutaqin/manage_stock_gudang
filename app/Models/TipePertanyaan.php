<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipePertanyaan extends Model
{
    protected $fillable = ['tipe_pertanyaan'];
}
