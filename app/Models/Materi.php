<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    protected $dates = ['tanggal'];
    protected $fillable = ['user_id', 'kelas_id', 'mapel_id', 'nama_materi', 'keterangan', 'pertemuan', 'video', 'banner', 'foto', 'tanggal'];

    // relasi mateeri ke user (one to many atau materi di miliki 1 guru)
    public function guru()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // relasi many to one role ke user (satu role bisa di miliki banyak users)
    public function tugas_s(){
        return $this->hasMany(Tugas::class);    
    }

    // relasi many to one role ke user (satu role bisa di miliki banyak users)
    public function soal_s(){
        return $this->hasMany(Soal::class);    
    }
}