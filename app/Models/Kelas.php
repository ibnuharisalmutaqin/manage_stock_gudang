<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $fillable = ['level_id', 'tahun_id', 'nama_kelas', 'slug', 'file'];

    // relasi many to many kelas dengan siswa
    public function siswa_s(){
        return $this->belongsToMany('App\Models\User', 'kelas_siswa')->withTimeStamps()->withPivot(['id'],['user_id'], ['kelas_id'], ['is_active']);
    }

    public function mapel_s(){
        return $this->belongsToMany('App\Models\Mapel', 'kelas_mapel')->withTimeStamps()->withPivot(['id'],['mapel_id'], ['kelas_id']);
    }

    // relasi user ke role (one to many atau 1 kelas hanya punya 1 tahun pelajaran)
    public function tahun()
    {
        return $this->belongsTo(Tahun::class);
    }
}
