<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Tahun;

class TahunController extends Controller
{
    public function index(){
        if(session('role') === 'Operator'){
            $tahun_s = Tahun::all();
            return view('backend/dir_tahun/index', compact('tahun_s'));
        }else{
            return back();
        }
    }

    public function store(Request $request){
        if(session('role') === 'Operator'){
            $tahun_old = Tahun::where('is_active', 1)->first();
            $tahun_old->is_active = 0;
            $tahun_old->save();
            $tahun = Tahun::create($request->all());
            return redirect()->route('indexTahunAjar');
        }else{
            return back();
        }
    }
    
    public function update(Request $request, $tahun_id){
        if(session('role') === 'Operator'){
            $tahun = Tahun::find($tahun_id);
            $tahun->update($request->all());
            return redirect()->route('indexTahunAjar');
        }else{
            return back();
        }
    }

    public function active($tahun_id){
        if(session('role') === 'Operator'){
            $tahun = Tahun::find($tahun_id);
            $tahun_old = Tahun::where('is_active', 1)->first();
            $tahun_old->is_active = 0;
            $tahun_old->save();
            $tahun->is_active = 1;
            $tahun->save();
            return redirect()->route('indexTahunAjar');
        }else{
            return back();
        }
    }
}
