<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\User;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session('role') === 'Operator'){
            $guru_s = User::where('role_id', 2)->get();
            return view('backend/dir_guru/index', compact('guru_s'));
        }else{
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('role') === 'Operator'){
            $action = 'create';
            return view('backend/dir_guru/action', compact('action'));
        }else{
            return back();
        }
    }

    public function store(Request $request)
    {
        $guru = User::create($request->all());
        $guru->slug = Str::of($request->name)->slug('-') . '_' . $guru->id;
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $newName = 'profile_' . $guru->id . ' ' . $filename;
            $path = Storage::putFileAs('public/profiles', $request->file('file'), $newName);
            $guru->foto = $newName;
        }
        $guru->save();
        return redirect()->route('indexGuru');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $guru = User::find($id);
        $action = 'edit';
        return view('backend/dir_guru/action', compact('action', 'guru'));
    }

    public function update(Request $request, $id)
    {
        $guru = User::find($id);
        $oldName = $guru->foto;
        $guru->update($request->all());
        $guru->slug = Str::of($request->name)->slug('-') . '_' . $guru->id;
        if($request->hasFile('file')){
            Storage::delete('public/profiles/'.$oldName);
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $newName = 'profile_' . $guru->id . ' ' . $filename;
            $path = Storage::putFileAs('public/profiles', $request->file('file'), $newName);
            $guru->foto = $newName;
        }
        $guru->save();
        return redirect()->route('indexGuru');
    }

    public function inactive($id){
        if(session('role') === 'Operator'){
            $guru = User::find($id);
            $guru->is_active = 0;
            $guru->save();
            return redirect()->route('indexGuru');
        }else{
            return back();
        }
    }

    public function active($id){
        if(session('role') === 'Operator'){
            $guru = User::find($id);
            $guru->is_active = 1;
            $guru->save();
            return redirect()->route('indexGuru');
        }else{
            return back();
        }
    }

    public function reset($id){
        if(session('role') === 'Operator'){
            $guru = User::find($id);
            $guru->password = Hash::make('passdefault');
            $guru->save();
            return redirect()->route('indexGuru');
        }else{
            return back();
        }
    }
    
    public function destroy($id)
    {
        //
    }
}
