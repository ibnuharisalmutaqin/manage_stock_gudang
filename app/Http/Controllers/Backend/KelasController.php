<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Kelas;
use App\Models\Level;
use App\Models\Hari;
use App\Models\Tahun;
use App\Models\Mapel;
use App\Models\Jadwal;
use App\Models\User;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session('role') === 'Operator'){
            $kelas_s = Kelas::all();
            return view('backend/dir_kelas/index', compact('kelas_s'));
        }else{
            return back();
        }
        
    }

    public function create()
    {
        if(session('role') === 'Operator'){
            $levels = Level::all();
            $tahun_s = Tahun::where('is_active', 1)->get();
            $action = 'create';
            return view('backend/dir_kelas/action', compact('action', 'levels', 'tahun_s'));
        }else{
            return back();
        }
    }

    public function store(Request $request)
    {
        if(session('role') === 'Operator'){
            $kelas = Kelas::create($request->all());
            $kelas->slug = Str::of($request->nama_kelas)->slug('-') . '_' . $kelas->id;
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $newName = 'Kelas_' . $kelas->id . ' ' . $filename;
                $path = Storage::putFileAs('public/kelas', $request->file('file'), $newName);
                $kelas->file = $newName;
            }
            $kelas->save();
            return redirect()->route('indexKelas');
        }else{
            return back();
        }
    }

    public function show($id)
    {
        if(session('role') === 'Operator'){
            $kelas = Kelas::find($id);
            return view('backend/dir_kelas/show', compact('kelas'));
        }else{
            return back();
        }
    }

    public function edit($id)
    {
        if(session('role') === 'Operator'){
            $levels = Level::all();
            $kelas = Kelas::find($id);
            $action = 'edit';
            return view('backend/dir_kelas/action', compact('action', 'levels', 'kelas'));
        }else{
            return back();
        }
    }

    public function update(Request $request, $id)
    {
        if(session('role') === 'Operator'){
            $kelas = Kelas::find($id);
            $oldName = $kelas->file;
            $kelas->update($request->all());
            $kelas->slug = Str::of($request->nama_kelas)->slug('-') . '_' . $kelas->id;
            if($request->hasFile('file')){
                Storage::delete('public/kelas/'.$oldName);
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $newName = 'Kelas_' . $kelas->id . ' ' . $filename;
                $path = Storage::putFileAs('public/kelas', $request->file('file'), $newName);
                $kelas->file = $newName;
            }
            $kelas->save();
            return redirect()->route('indexKelas');
        }else{
            return back();
        }
    }

    public function destroy($id)
    {
        //
    }

    public function indexHariKelas($id){
        if(session('role') === 'Operator'){
            $haris = Hari::all();
            $kelas = Kelas::find($id);
            return view('backend/dir_kelas/indexHariKelas', compact('haris', 'kelas'));
        }else{
            return back();
        }
    }

    public function showHariKelas($kelas_id, $hari_id){
        if(session('role') === 'Operator'){
            $jadwal_s = Jadwal::where('kelas_id', $kelas_id)->where('hari_id', $hari_id)->get();
            $kelas = Kelas::find($kelas_id);
            $hari = Hari::find($hari_id);
            $tahun_s = Tahun::all();
            $mapel_s = Mapel::where('level_id', $kelas->level_id)->where('tahun_id', $kelas->tahun_id)->get();
            return view('backend/dir_kelas/showHariKelas', compact('hari', 'kelas', 'mapel_s', 'jadwal_s', 'tahun_s'));
        }else{
            return back();
        }
    }
    public function createSiswa($kelas_id){
        if(session('role') === 'Operator'){
            $action = 'create';
            return view('backend/dir_kelas/actionSiswa', compact('action', 'kelas_id'));
        }else{
            return back();
        }
    }

    public function storeSiswa(Request $request, $kelas_id){
        if(session('role') === 'Operator'){
            $siswa = User::create($request->all());
            $siswa->slug = Str::of($request->name)->slug('-') . '_' . $siswa->id;
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $newName = 'profile_' . $siswa->id . ' ' . $filename;
                $path = Storage::putFileAs('public/profiles', $request->file('file'), $newName);
                $siswa->foto = $newName;
            }
            $siswa->save();

            if($siswa->kelas_s()->where('kelas_id', $kelas_id )->exists()){
                return redirect()->route('showKelas', $kelas_id);
            }  
            $siswa->kelas_s()->attach($kelas_id);
            return redirect()->route('showKelas', $kelas_id);
        }else{
            return back();
        }
    }
}
