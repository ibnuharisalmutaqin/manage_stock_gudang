<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use Carbon;
use App\Models\Materi;
use App\Models\Hari;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Ebook;
use App\Models\Soal;

class SoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id)
    {
        if(session('role') === 'Guru'){
            $soal = Soal::create($request->all());
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id)
    {
        if(session('role') === 'Guru'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $soal = Soal::find($soal_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            return view('backend/dir_materi/showSoal', compact('soal', 'hari', 'materi', 'mapel', 'kelas', 'ebooks'));
        }else{
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id)
    {
        if(session('role') === 'Guru'){
            $soal = Soal::find($soal_id);
            $soal->update($request->all());
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id)
    {
        if(session('role') === 'Guru'){
            $soal = Soal::find($soal_id);
            $soal->delete($soal);
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully destroyed !!');
        }else{
            return back();
        }
    }
}
