<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Mapel;
use App\Models\Level;
use App\Models\Tahun;
use App\Models\User;

class MapelController extends Controller
{
    public function index()
    {
        if(session('role') === 'Operator'){
            $mapel_s = Mapel::all();
            return view('backend/dir_mapel/index', compact('mapel_s'));
        }else{
            return back();
        }
        
    }

    public function create()
    {
        if(session('role') === 'Operator'){
            $levels = Level::all();
            $tahun_s = Tahun::where('is_active', 1)->get();
            $action = 'create';
            return view('backend/dir_mapel/action', compact('action', 'levels', 'tahun_s'));
        }else{
            return back();
        }
    }

    public function store(Request $request)
    {
        if(session('role') === 'Operator'){
            $mapel = Mapel::create($request->all());
            $mapel->slug = Str::of($request->nama_mapel)->slug('-') . '_' . $mapel->id;
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $newName = 'Mapel_' . $mapel->id . ' ' . $filename;
                $path = Storage::putFileAs('public/mapels', $request->file('file'), $newName);
                $mapel->file = $newName;
            }
            $mapel->save();
            return redirect()->route('indexMapel');
        }else{
            return back();
        }
    }

    public function show($id)
    {
        if(session('role') === 'Operator'){
            $mapel = Mapel::find($id);
            $guru_s = User::where('role_id', 2)->get();
            return view('backend/dir_mapel/show', compact('mapel', 'guru_s'));
        }else{
            return back();
        }
    }

    public function edit($id)
    {
        if(session('role') === 'Operator'){
            $levels = Level::all();
            $mapel = Mapel::find($id);
            $action = 'edit';
            return view('backend/dir_mapel/action', compact('action', 'mapel', 'levels'));
        }else{
            return back();
        }
    }

    public function update(Request $request, $id)
    {
        if(session('role') === 'Operator'){
            $mapel = Mapel::find($id);
            $oldName = $mapel->file;
            $mapel->update($request->all());
            $mapel->slug = Str::of($request->nama_mapel)->slug('-') . '_' . $mapel->id;
            if($request->hasFile('file')){
                Storage::delete('public/mapels/'.$oldName);
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $newName = 'Mapel_' . $mapel->id . ' ' . $filename;
                $path = Storage::putFileAs('public/mapels', $request->file('file'), $newName);
                $mapel->file = $newName;
            }
            $mapel->save();
            return redirect()->route('indexMapel');
        }else{
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function insert($mapel_id, Request $request){
        if(session('role') === 'Operator'){
            $mapel = Mapel::find($mapel_id);
            if($mapel->guru_s()->where('user_id', $request->guru_id )->exists()){
                return redirect()->route('showMapel', $mapel->id);
            }  
            $mapel->guru_s()->attach($request->guru_id);
            return redirect()->route('showMapel', $mapel->id);
        }else{
            return back();
        }
    }

    public function out($mapel_id, $guru_id){
        if(session('role') === 'Operator'){
            $mapel = Mapel::find($mapel_id);
            $mapel->guru_s()->wherePivot('user_id', $guru_id)->detach();
            return redirect()->route('showMapel', $mapel->id);
        }else{
            return back();
        }
    }
}
