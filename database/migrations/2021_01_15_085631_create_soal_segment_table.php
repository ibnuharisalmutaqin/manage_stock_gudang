<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoalSegmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soal_segment', function (Blueprint $table) {
            $table->id();
            $table->foreignId('soal_id')->nullable()->constrained('soals')->onDelete('cascade');
            $table->foreignId('segment_id')->nullable()->constrained('segments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soal_segment');
    }
}
