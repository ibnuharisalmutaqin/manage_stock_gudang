@extends('layouts.main')
@section('title', 'App_School')
@section('content')
<div class="row mt-4">
  <div class="col-12 col-xl-12">
    <div class="card card-body bg-white border-light shadow-sm mb-4">
      <img src="{{ asset('storage/pengumuman_s/'. $pengumuman->foto) }}" class="card-img-top rounded" alt="blog image">
      <h4 class="mt-4">{{ $pengumuman->nama_pengumuman }}</h4>

      <div class="mt-1">
        <h4 class="h6 font-weight-normal text-gray mb-3 mb-sm-0 small"><span class="fas fa-clock mr-2"></span>{{ date('d F Y', strtotime($pengumuman->waktu)) }}</h4>
      </div>
      <p class="card-text my-4">{!! nl2br($pengumuman->keterangan) !!}</p>
    </div>
  </div>
</div>
@endsection