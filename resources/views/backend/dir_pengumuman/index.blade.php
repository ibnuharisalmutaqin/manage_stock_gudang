@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
@if(session('role') === 'Operator')
<div class="table-settings my-4">
  <div class="row">
    <div class="col-12">
      <a href="{{ route('createPengumuman')}}" class="btn btn-primary">Tambah Pengumuman</a>
    </div>
  </div>
</div>
@endif
<div class="row">
  @foreach ($pengumumans as $pengumuman)
  <div class="col-12 col-lg-6">
    <div class="card border-light shadow-sm">
      <div class="card-body">
        <div class="row align-items-center d-block d-sm-flex">
          <div class="col-auto mb-3 mb-sm-0">
            <div class="calendar d-flex">
              <span class="calendar-month"></span><span class="calendar-day"><span class="fas fa-volume-up"></span></span>
            </div>
          </div>
          <div class="col">
            <a href="{{ route('showPengumuman', $pengumuman->id)}}"> <h3 class="h5">{{$pengumuman->nama_pengumuman}}</h3></a>
            <div class="small font-weight-bold mt-1">{{ date('d F Y', strtotime($pengumuman->waktu)) }}</div>
            @if(session('role') === 'Operator')
            <a href="{{ route('editPengumuman', $pengumuman->id)}}" class="btn btn-primary btn-sm mr-2 small font-weight-bold mt-3 py-1">Edit</a>
            <a href="{{ route('destroyPengumuman', $pengumuman->id)}}" class="btn btn-primary btn-sm mr-2 small font-weight-bold mt-3 py-1">Hapus</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>

{{-- konten --}}
@endsection