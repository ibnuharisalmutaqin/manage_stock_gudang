@extends('layouts.main')
@section('title', 'App_School')
@section('content')
<div class="row mt-4">
  <div class="col-12 col-xl-9">
    <div class="card card-body bg-white border-light shadow-sm mb-4">
      @if($materi->banner == 1 && $materi->foto !== NULL)
        <img src="{{ asset('storage/materi_s/'. $materi->foto) }}" alt="{{ asset('storage/materi_s/'. $materi->foto) }}" class="card-img-top rounded" alt="blog image">
      @elseif($materi->banner == 0 && $materi->video !== NULL)
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item rounded-top" src="https://www.youtube.com/embed/{{$materi->video}}" allowfullscreen></iframe>
        </div>
      @endif
      <h4 class="mt-4">{{ $materi->nama_materi }}</h4>
      @if($materi->pertemuan !== NULL)
      <a href="{{ (is_null(parse_url($materi->pertemuan, PHP_URL_HOST)) ? '//' : '').$materi->pertemuan }}" target="_blank" class="animate-up-2">
        <div class="mt-1">
          <h4 class="h6 font-weight-normal text-gray mb-3 mb-sm-0 small"><span class="fas fa-clock mr-2"></span>Pertemuan Via Online</h4>
        </div>
      </a>
      @endif
      <p class="card-text my-4">{!! nl2br($materi->keterangan) !!}</p>
    </div>
  </div>
  @include('backend/dir_materi/side')
</div>
@endsection