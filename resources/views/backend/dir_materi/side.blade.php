<div class="col-12 col-xl-3">
  <div class="col-12 px-0 mb-4">
    {{-- modal buat ebook --}}
    <div class="modal fade" id="modal-create-ebook" tabindex="-1" role="dialog" aria-labelledby="modal-create-ebook" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body p-0">
            <div class="card border-light p-4">
              <div class="card-header text-center pb-0">
                <h2 class="mb-0 h5">Buat Ebook</h2>
              </div>
              <div class="card-body">
                <form action="{{ route('storeEbook', [$hari->slug, $kelas->id, $mapel->id, $materi->id]) }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                  <input type="hidden" name="materi_id" value="{{ $materi->id }}">
                  <div class="form-group my-4">
                    <label for="file">Ebook</label>
                    <div class="file-field">
                      <div class="d-flex ml-xl-3">
                        <div class="d-flex"><span class="icon icon-md"><span class="fas fa-paperclip mr-3"></span></span>
                          <input type="file" id="file" name="file">
                          <div class="d-md-block text-left">
                            <div class="font-weight-normal text-dark mb-1">Pilih Dokumen</div>
                            <div class="text-gray small">PDF, PPT or DOC. Max size of 8MB</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-block btn-primary">Insert</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="card border-light shadow-sm">
      <div class="card-body">
        @if(session('role') === 'Guru')
        <button data-toggle="modal" data-target="#modal-create-ebook" class="btn btn-sm btn-secondary py-0 mb-1 px-3">Create</button>
        @endif
        <p>Mendownload Ebook</p>
        @foreach ($ebooks as $ebook)
        <div class="d-block py-1">
          {{-- modal update mapel --}}
          <div class="modal fade" id="modal-edit-ebook{{$loop->iteration}}" tabindex="-1" role="dialog" aria-labelledby="modal-edit-ebook{{$loop->iteration}}" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-body p-0">
                  <div class="card border-light p-4">
                    <div class="card-header text-center pb-0">
                      <h2 class="mb-0 h5">Edit Ebook</h2>
                    </div>
                    <div class="card-body">
                      <form action="{{ route('updateEbook', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $ebook->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <input type="hidden" name="materi_id" value="{{ $materi->id }}">
                        <div class="form-group my-4">
                          <label for="file">Ebook</label>
                          <div class="file-field">
                            <div class="d-flex ml-xl-3">
                              <div class="d-flex"><span class="icon icon-md"><span class="fas fa-paperclip mr-3"></span></span>
                                <input type="file" id="file" name="file">
                                <div class="d-md-block text-left">
                                  <div class="font-weight-normal text-dark mb-1">Pilih Dokumen</div>
                                  <div class="text-gray small">PDF, PPT or DOC. Max size of 8MB</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary">Update</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="d-flex align-items-center pt-1">
            <a href="{{ route('downloadEbook', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $ebook->id]) }}" class="scale-up-2">
              <div class="icon icon-shape icon-sm icon-shape-danger rounded mr-3"><span class="fas fa-book p-1"></span></div>
            </a>
            <div class="d-block"><label class="mb-0">Ebook {{ $loop->iteration }}</label></div>
            @if(session('role') === 'Guru')
              <div class="btn-group ml-auto">
                <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                  <button data-toggle="modal" data-target="#modal-edit-ebook{{$loop->iteration}}" class="dropdown-item"><span class="fas fa-user-shield mr-2"></span>Edit</button>
                  <a class="dropdown-item text-danger" href="{{ route('destroyEbook', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $ebook->id]) }}"><span class="fas fa-user-times mr-2"></span>Delete</a>
                </div>
              </div>
            @endif
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>

  <div class="col-12 px-0 mb-4">
    {{-- modal buat tugas --}}
    <div class="modal fade" id="modal-create-tugas" tabindex="-1" role="dialog" aria-labelledby="modal-create-tugas" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body p-0">
            <div class="card border-light p-4">
              <div class="card-header text-center pb-0">
                <h2 class="mb-0 h5">Buat Tugas</h2>
              </div>
              <div class="card-body">
                <form action="{{ route('storeTugas', [$hari->slug, $kelas->id, $mapel->id, $materi->id]) }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                  <input type="hidden" name="materi_id" value="{{ $materi->id }}">
                  <div class="row">
                    <div class="col-md-12 mb-3">
                      <div>
                        <label for="nama_tugas">Judul Tugas</label>
                        <input name="nama_tugas" class="form-control" id="nama_tugas" type="text" placeholder="Masukkan Judul" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <div class="form-group">
                        <label for="start">Mulai Mengerjakatn</label>
                        <input name="start" class="form-control" id="start" type="datetime-local" required>
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <div class="form-group">
                        <label for="expired">Terakir Mengerjakan</label>
                        <input name="expired" class="form-control" id="expired" type="datetime-local" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 mb-3">
                      <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea name="keterangan" class="form-control" placeholder="Masukkan Deskripsi." id="deskripsi" rows="8"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group mb-3">
                    <label for="file">Pentunjuk Lebih Detail</label>
                    <div class="file-field">
                      <div class="d-flex ml-xl-3">
                        <div class="d-flex"><span class="icon icon-md"><span class="fas fa-paperclip mr-3"></span></span>
                          <input type="file" id="file" name="file">
                          <div class="d-md-block text-left">
                            <div class="font-weight-normal text-dark mb-1">Pilih Dokumen</div>
                            <div class="text-gray small">PDF, PPT or DOC. Max size of 8MB</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-block btn-primary">Insert</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="card border-light shadow-sm">
      <div class="card-body">
        @if(session('role') === 'Guru')
        <button data-toggle="modal" data-target="#modal-create-tugas" class="btn btn-sm btn-secondary py-0 mb-1 px-3">Create</button>
        @endif
        <p>Mengumpulkan Tugas</p>
        <div class="d-block">
          @foreach ($materi->tugas_s as $tugas)
          {{-- modal edit tugas --}}
          <div class="modal fade" id="modal-edit-tugas{{$loop->iteration}}" tabindex="-1" role="dialog" aria-labelledby="modal-edit-tugas" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-body p-0">
                  <div class="card border-light p-4">
                    <div class="card-header text-center pb-0">
                      <h2 class="mb-0 h5">Edit Tugas</h2>
                    </div>
                    <div class="card-body">
                      <form action="{{ route('updateTugas', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $tugas->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <input type="hidden" name="materi_id" value="{{ $materi->id }}">
                        <div class="row">
                          <div class="col-md-12 mb-3">
                            <div>
                              <label for="nama_tugas">Judul Tugas</label>
                              <input name="nama_tugas" class="form-control" id="nama_tugas" type="text" placeholder="Masukkan Judul" value="{{ $tugas->nama_tugas }}" required>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 mb-3">
                            <div class="form-group">
                              <label for="start">Mulai Mengerjakan</label>
                              <input name="start" class="form-control" id="start" type="datetime-local" value="{{ date('Y-m-d\TH:i', strtotime($tugas->start)) }}" required>
                            </div>
                          </div>
                          <div class="col-md-6 mb-3">
                            <div class="form-group">
                              <label for="expired">Terakir Mengerjakan</label>
                              <input name="expired" class="form-control" id="expired" type="datetime-local" value="{{ date('Y-m-d\TH:i', strtotime($tugas->expired)) }}" required>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12 mb-3">
                            <div class="form-group">
                              <label for="deskripsi">Deskripsi</label>
                              <textarea name="keterangan" class="form-control" placeholder="Masukkan Deskripsi." id="deskripsi" rows="8">{{ $tugas->keterangan }}</textarea>
                            </div>
                          </div>
                        </div>
                        <div class="form-group mb-3">
                          <label for="file">Pentunjuk Lebih Detail</label>
                          <div class="file-field">
                            <div class="d-flex ml-xl-3">
                              <div class="d-flex"><span class="icon icon-md"><span class="fas fa-paperclip mr-3"></span></span>
                                <input type="file" id="file" name="file">
                                <div class="d-md-block text-left">
                                  <div class="font-weight-normal text-dark mb-1">Pilih Dokumen</div>
                                  <div class="text-gray small">PDF, PPT or DOC. Max size of 8MB</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary">Insert</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {{-- loop list tugas --}}
          <div class="d-flex align-items-center py-1">
            <a href="{{ route('indexTugas', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $tugas->id]) }}" class="scale-up-2">
              <div class="icon icon-shape icon-sm icon-shape-danger rounded mr-3">
                <span class="fas fa-paperclip"></span>
              </div>
            </a>
            <div class="d-block"><label class="mb-0">Tugas {{$loop->iteration}}</label>
              @if(session('role') === 'Guru')
              <h6 class="mb-0 text-success"><b> {{ $tugas->user_s()->count()}} complete</b></h6>
              @elseif($tugas->user_s()->where('user_id', Auth::user()->id)->exists())
                @if($tugas->user_s()->where('user_id', Auth::user()->id)->first()->pivot->status == 1)
                <h6 class="mb-0">Nilai_ <span class="h4 ">"{{$tugas->user_s()->where('user_id', Auth::user()->id)->first()->pivot->nilai}}"</span></h6>
                @else
                <h6 class="mb-0 text-success">Complete</h6>
                @endif
              @else
              <h6 class="mb-0 text-danger">Incomplete</h6>
              @endif
            </div>
            @if(session('role') === 'Guru')
              <div class="btn-group ml-auto">
                <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="{{ route('showTugas', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $tugas->id])}}"><span class="fas fa-eye mr-2"></span>Review</a> 
                  <button data-toggle="modal" data-target="#modal-edit-tugas{{$loop->iteration}}" class="dropdown-item"><span class="fas fa-user-shield mr-2"></span>Edit</button>
                  <a class="dropdown-item text-danger" href="{{ route('destroyTugas', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $tugas->id]) }}"><span class="fas fa-user-times mr-2"></span>Delete</a>
                </div>
              </div>
            @endif
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

  <div class="col-12 px-0 mb-4">
    {{-- modal buat soal --}}
    <div class="modal fade" id="modal-create-soal" tabindex="-1" role="dialog" aria-labelledby="modal-create-soal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body p-0">
            <div class="card border-light p-4">
              <div class="card-header text-center pb-0">
                <h2 class="mb-0 h5">Buat Soal</h2>
              </div>
              <div class="card-body">
                <form action="{{ route('storeSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id]) }}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                  <input type="hidden" name="materi_id" value="{{ $materi->id }}">
                  <div class="row">
                    <div class="col-md-12 mb-3">
                      <div>
                        <label for="nama_soal">Judul Soal</label>
                        <input name="nama_soal" class="form-control" id="nama_soal" type="text" placeholder="Masukkan Judul" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 mb-3">
                      <div class="form-group">
                        <label for="start">Mulai Mengerjakatn</label>
                        <input name="start" class="form-control" id="start" type="datetime-local" required>
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <div class="form-group">
                        <label for="expired">Terakir Mengerjakan</label>
                        <input name="expired" class="form-control" id="expired" type="datetime-local" required>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-block btn-primary">Insert</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card border-light shadow-sm">
      <div class="card-body">
        @if(session('role') === 'Guru')
        <button data-toggle="modal" data-target="#modal-create-soal" class="btn btn-sm btn-secondary py-0 mb-1 px-3">Create</button>
        @endif
        <p>Mengerjakan Soal-Soal</p>
        <div class="d-block">
          @foreach ($materi->soal_s as $soal)
          {{-- modal edit tugas --}}
          <div class="modal fade" id="modal-edit-soal{{$loop->iteration}}" tabindex="-1" role="dialog" aria-labelledby="modal-edit-tugas" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-body p-0">
                  <div class="card border-light p-4">
                    <div class="card-header text-center pb-0">
                      <h2 class="mb-0 h5">Edit Soal</h2>
                    </div>
                    <div class="card-body">
                      <form action="{{ route('updateSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <input type="hidden" name="materi_id" value="{{ $materi->id }}">
                        <div class="row">
                          <div class="col-md-12 mb-3">
                            <div>
                              <label for="nama_soal">Judul Soal</label>
                              <input name="nama_soal" class="form-control" id="nama_soal" type="text" placeholder="Masukkan Judul" value="{{ $soal->nama_soal}}" required>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 mb-3">
                            <div class="form-group">
                              <label for="start">Mulai Mengerjakan</label>
                              <input name="start" class="form-control" id="start" type="datetime-local" value="{{ date('Y-m-d\TH:i', strtotime($soal->start)) }}" required>
                            </div>
                          </div>
                          <div class="col-md-6 mb-3">
                            <div class="form-group">
                              <label for="expired">Terakir Mengerjakan</label>
                              <input name="expired" class="form-control" id="expired" type="datetime-local" value="{{ date('Y-m-d\TH:i', strtotime($soal->expired)) }}" required>
                            </div>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary">Insert</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="d-flex align-items-center pt-1">
            <a @if(session('role') === 'Guru') href="{{ route('indexSettingSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id])}}" @elseif($soal->user_s()->where('user_id', Auth::user()->id)->exists()) @else href="{{ route('kerjakanSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id])}}" @endif class="scale-up-2">
              <div class="icon icon-shape icon-sm icon-shape-danger rounded mr-3">
                <span class="fas fa-tasks"></span>
              </div>
            </a>
            <div class="d-block"><label class="mb-0">Soal {{$loop->iteration}}</label>
              @if(session('role') === 'Guru')
              <h6 class="mb-0 text-success"><b> {{ $soal->user_s()->count()}} complete</b></h6>
              @elseif($soal->user_s()->where('user_id', Auth::user()->id)->exists())
                  <h6 class="mb-0">Nilai_ <span class="h4 ">"{{ $soal->user_s()->where('user_id', Auth::user()->id)->first()->pivot->nilai }}"</span></h6>
              @else
              <h6 class="mb-0 text-danger">Incomplete</h6>
              @endif
            </div>
            @if(session('role') === 'Guru')
              <div class="btn-group ml-auto">
                <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="{{ route('showSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id])}}"><span class="fas fa-eye mr-2"></span>Review</a>
                  <button data-toggle="modal" data-target="#modal-edit-soal{{$loop->iteration}}" class="dropdown-item"><span class="fas fa-user-shield mr-2"></span>Edit</button>
                  <a class="dropdown-item text-danger" href="{{ route('destroySoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id]) }}"><span class="fas fa-user-times mr-2"></span>Delete</a>
                </div>
              </div>
            @endif
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>