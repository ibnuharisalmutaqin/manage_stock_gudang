@extends('layouts.main')
@section('title', 'App_School')
@section('content')
<div class="row mt-4">
  
  <div class="col-12 col-xl-9">
    <div class="card border-light shadow-sm mb-4">
      <div class="card border-light shadow-sm">
        <div class="card-body">
          <div class="row align-items-center d-block d-sm-flex">
            <div class="col-auto mb-3 mb-sm-0">
              <div class="calendar d-flex">
                <span class="calendar-month">Soal</span><span class="calendar-day">10</span>
              </div>
            </div>
            <div class="col">
              <h3 class="h5">{{$segment->nama_segment}}</h3>
              <div class="small font-weight-bold mt-1">10 Soal Pertanyaan</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-buat-pertanyaan" tabindex="-1" role="dialog" aria-labelledby="modal-buat-pertanyaan" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body p-0">
            <div class="card border-light p-4">
              <div class="card-body">
                <form action="{{ route('storePertanyaan', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id, $segment->id])}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="row">
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    <input type="hidden" name="mapel_id" value="{{$mapel->id}}">
                    <div class="col-md-12 mb-3">
                      <div>
                        <label for="pertanyaan">Pertanyaan</label>
                        <input name="pertanyaan" class="form-control" id="pertanyaan" type="text" placeholder="Pertanyaan" value="" required>
                      </div>
                    </div>
                    <div class="col-md-12 mb-3">
                      <div>
                        <label for="state">Tipe Pertanyaan</label>
                        <select id="state" class="w-100" name="tipe_pertanyaan_id">
                          @foreach ($tipe_pertanyaans as $tipe_pertanyaan)
                          <option value="{{ $tipe_pertanyaan->id }}">{{ $tipe_pertanyaan->tipe_pertanyaan }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12 mb-3">
                      <div>
                        <label for="pilihan">Pilihan</label>
                        <textarea name="pilihan" class="form-control" placeholder="Masukkan pilihan." id="pilihan" rows="5"></textarea>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="table-settings mb-3">
      <div class="row align-items-center justify-content-between">
        <div class="col col-md-6 col-lg-3 col-xl-4">
          <div class="btn-toolbar dropdown">
            <button data-toggle="modal" data-target="#modal-buat-pertanyaan" class="btn btn-primary btn-sm mr-2"><span class="fas fa-plus mr-2"></span>Buat Pertanyaan</button>
          </div>
        </div>
        <div class="col-4 col-md-2 col-xl-1 pl-md-0 text-right">
          <div class="btn-group">
            <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="icon icon-sm icon-gray"><span class="fas fa-cog"></span></span><span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
              <span class="dropdown-item font-weight-bold text-dark">Show</span>
              <a class="dropdown-item d-flex font-weight-bold" href="#">10 <span class="icon icon-small ml-auto"><span class="fas fa-check"></span></span></a>
              <a class="dropdown-item font-weight-bold" href="#">20</a> <a class="dropdown-item font-weight-bold" href="#">30</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="card card-body border-light shadow-sm table-wrapper table-responsive pt-0 mb-4">
      <table class="table user-table table-hover align-items-center">
        <thead>
          <tr>
            <th class="border-bottom py-4">Pertanyaan</th>
            <th class="border-bottom py-4"></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($segment->pertanyaans as $pertanyaan)
          <div class="modal fade" id="modal-edit-pertanyaan{{$loop->iteration}}" tabindex="-1" role="dialog" aria-labelledby="modal-edit-pertanyaan{{$loop->iteration}}" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-body p-0">
                  <div class="card border-light p-4">
                    <div class="card-body">
                      <form action="{{ route('updatePertanyaan', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id, $segment->id, $pertanyaan->id])}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                          <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                          <input type="hidden" name="mapel_id" value="{{$mapel->id}}">
                          <div class="col-md-12 mb-3">
                            <div>
                              <label for="pertanyaan">Pertanyaan</label>
                              <input name="pertanyaan" class="form-control" id="pertanyaan" type="text" placeholder="Pertanyaan" value="{{$pertanyaan->pertanyaan}}" required>
                            </div>
                          </div>
                          <div class="col-md-12 mb-3">
                            <div>
                              <label for="tipe_pertanyaan_id">Tipe Pertanyaan</label>
                              <select name="tipe_pertanyaan_id" id="tipe_pertanyaan_id" class="form-select" aria-label="Default select example">
                                @foreach ($tipe_pertanyaans as $tipe_pertanyaan)
                                <option value="{{ $tipe_pertanyaan->id }}" @if($pertanyaan->tipe_pertanyaan_id == $tipe_pertanyaan->id) selected @endif>{{ $tipe_pertanyaan->tipe_pertanyaan }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-md-12 mb-3">
                            <div>
                              <label for="pilihan">Pilihan</label>
                              <textarea name="pilihan" class="form-control" placeholder="Masukkan pilihan." id="pilihan" rows="5">{{$pertanyaan->pilihan}}</textarea>
                            </div>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary">Submit</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

            <tr>
              <td>
                <div class="d-flex align-items-center">
                  <div class="user-avatar bg-secondary mr-3">{{$loop->iteration}}</div>
                  <div class="d-block">
                    <span class="font-weight-bold">{{$pertanyaan->pertanyaan}}</span>
                    <div class="small text-gray">{{$pertanyaan->tipe->tipe_pertanyaan}}</div>
                  </div>
                </div>
              </td>
              <td class="text-right">
                <div class="btn-group">
                  <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu">
                    <a data-toggle="modal" data-target="#modal-edit-pertanyaan{{$loop->iteration}}" class="dropdown-item"><span class="fas fa-user-shield mr-2"></span>Edit Pertanyaan</a>
                    <a class="dropdown-item text-danger" href="{{ route('destroyPertanyaan', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id, $segment->id, $pertanyaan->id])}}"><span class="fas fa-user-times mr-2"></span>Hapus Permanent</a>
                  </div>
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      <div class="card-footer pb-3 border-0 d-flex align-items-center justify-content-between mt-5 pt-5">
        <nav aria-label="Page navigation example">
          <ul class="pagination mb-0">
            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item active"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">4</a></li>
            <li class="page-item"><a class="page-link" href="#">5</a></li>
            <li class="page-item"><a class="page-link" href="#">Next</a></li>
          </ul>
        </nav>
      </div>
    </div>
    
  </div>
  
  @include('backend/dir_materi/side')
</div>
@endsection