@extends('layouts.main')
@section('title', 'App_School')
@section('content')
<div class="row mt-4">
  <div class="col-12 col-xl-8">
    <div class="card card-body bg-white border-light shadow-sm mb-4">
      <h2 class="h5 mb-4">General information</h2>
      <form>
        <div class="row">
          <div class="col-md-12 mb-3">
            <div>
              <label for="first_name">Nama Guru</label>
              <input name="name" class="form-control" id="first_name" type="text" placeholder="Enter your name" required>
            </div>
          </div>
        </div>
        <div class="row align-items-center">
          <div class="col-md-6 mb-3">
            <label for="birthday">Birthday</label>
            <div class="input-group">
              <span class="input-group-text"><span class="far fa-calendar-alt"></span></span>
              <input name="tanggal_lahir" data-datepicker="" class="form-control" id="birthday" type="text" placeholder="dd/mm/yyyy" required>
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="gender">Gender</label>
            <select name="jenis_kelamin" class="form-select mb-0" id="gender" aria-label="Gender select example">
              <option selected="selected">Gender</option>
              <option value="Laki Laki">Laki Laki</option>
              <option value="Perempuan">Perempuan</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <div class="form-group">
              <label for="email">Email</label>
              <input name="email" class="form-control" id="email" type="email" placeholder="name@gmail.com" required>
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <div class="form-group">
              <label for="phone">Phone</label>
              <input name="phone" class="form-control" id="phone" type="number" placeholder="+12-345 678 910" required>
            </div>
          </div>
        </div>
        <h2 class="h5 my-4">Adress</h2>
        <div class="row">
          <div class="col-sm-12 mb-3">
            <div class="form-group">
              <label for="address">Address</label>
              <input name="alamat" class="form-control" id="address" type="text" placeholder="Enter your home address" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4 mb-3">
            <div class="form-group">
              <label for="city">City</label>
              <input name="kota" class="form-control" id="city" type="text" placeholder="City" required>
            </div>
          </div>
          <div class="col-sm-4 mb-3">
            <div class="form-group">
              <label for="number">Number</label>
              <input name="nomor" class="form-control" id="number" type="number" placeholder="No." required>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="zip">ZIP</label>
              <input name="kode_pos" class="form-control" id="zip" type="tel" placeholder="ZIP" required>
            </div>
          </div>
        </div>

        <h2 class="h5 my-4">Instansi</h2>
        <div class="row">
          <div class="col-sm-6 mb-3">
            <div class="form-group">
              <label for="nip_guru">NIP Guru</label>
              <input name="nip_guru" class="form-control" id="nip_guru" type="text" placeholder="Enter your NIP" required>
            </div>
          </div>
          <div class="col-sm-6 mb-3">
            <div class="form-group">
              <label for="nik">NIK WNI </label>
              <input name="nik" class="form-control" id="nik" type="text" placeholder="NIK KTP" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4 mb-3">
            <div class="form-group">
              <label for="jabatan">Jabatan</label>
              <input name="jabatan" class="form-control" id="jabatan" type="text" placeholder="Jabatan Disekolah" required>
            </div>
          </div>
          <div class="col-sm-4 mb-3">
            <div class="form-group">
              <label for="status">Status</label>
              <input name="status" class="form-control" id="status" type="text" placeholder="Status Pernikahan" required>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="Pendidikan">Pendidikan</label>
              <input name="pendidikan" class="form-control" id="Pendidikan" type="tel" placeholder="Pendidikan" required>
            </div>
          </div>
        </div>
        <div class="mt-3"><button type="submit" class="btn btn-primary">Save All</button></div>
      </form>
    </div>
  </div>
  <div class="col-12 col-xl-4">
    <div class="row">
      <div class="col-12 mb-4">
        <div class="card border-light text-center p-0">
          <div class="profile-cover rounded-top" data-background="https://png.pngtree.com/thumb_back/fh260/background/20190828/pngtree-dark-vector-abstract-background-image_302715.jpg"></div>
          <div class="card-body pb-5"><img src="https://api.duniagames.co.id/api/content/upload/file/8143860661599124172.jpg"
              class="user-avatar large-avatar rounded-circle mx-auto mt-n7 mb-4" alt="Neil Portrait">
            <h4 class="h3">Nama</h4>
            <h5 class="font-weight-normal">NIS / NIP</h5>
            <p class="text-gray mb-4">ROLE</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection