@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="row mt-4">
  <div class="col-12 col-xl-12">
    <div class="card card-body bg-white border-light shadow-sm mb-4">
      <h2 class="h5 mb-4">Form Action</h2>
      
      <form @if($action == 'create') action="{{ route('storeSiswaKelas', $kelas_id) }}" @else action="{{ route('updateGuru', $guru->id) }}" @endif method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" value="3" name="role_id">
        <div class="row">
          <div class="col-md-12 mb-3">
            <div>
              <label for="name">Nama Siswa</label>
              <input name="name" class="form-control" id="name" type="text" placeholder="Enter the name" @if($action == 'edit') value="{{ $siswa->name }}" @endif required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <div class="form-group">
              <label for="email">Email</label>
              <input name="email" class="form-control" id="email" type="email" placeholder="name@gmail.com" @if($action == 'edit') value="{{ $siswa->email }}" @endif required>
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <div class="form-group">
              <label for="phone">Phone</label>
              <input name="phone" class="form-control" id="phone" type="number" placeholder="081306787910" @if($action == 'edit') value="{{ $siswa->phone }}" @endif required>
            </div>
          </div>
        </div>

        <h2 class="h5 my-4">Instansi</h2>
        <div class="row">
          <div class="col-sm-4 mb-3">
            <div class="form-group">
              <label for="nis_siswa">NIS Siswa</label>
              <input name="nis_siswa" class="form-control" id="nis_siswa" type="text" placeholder="NIS Siswa" @if($action == 'edit') value="{{ $siswa->nis_siswa }}" @endif required>
            </div>
          </div>
          <div class="col-sm-4 mb-3">
            <div class="form-group">
              <label for="alamat">Alamat </label>
              <input name="alamat" class="form-control" id="alamat" type="text" placeholder="Alamat Siswa" @if($action == 'edit') value="{{ $siswa->alamat }}" @endif required>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="jenis_kelamin">Jenis_Kelamin</label>
              <select id="state" class="w-100" name="jenis_kelamin">
                <option value="Laki-Laki">Laki-Laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 mb-3">
            <div class="form-group">
              <label for="file">Foto Profile</label>
              <div class="file-field">
                <div class="d-flex ml-xl-3">
                  <div class="d-flex"><span class="icon icon-md"><span class="fas fa-paperclip mr-3"></span></span>
                    <input type="file" id="file" name="file">
                    <div class="d-md-block text-left">
                      <div class="font-weight-normal text-dark mb-1">Choose Image</div>
                      <div class="text-gray small">JPG, GIF or PNG. Max size of 800K</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="mt-3"><button type="submit" class="btn btn-primary">Save All</button></div>
      </form>
    </div>
  </div>
</div>
{{-- konten --}}
@endsection