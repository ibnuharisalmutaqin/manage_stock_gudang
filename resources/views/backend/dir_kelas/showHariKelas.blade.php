@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="modal fade" id="modal-form-signup" tabindex="-1" role="dialog" aria-labelledby="modal-form-signup" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body p-0">
        <div class="card border-light p-4">
          <div class="card-header text-center pb-0">
            <h2 class="mb-0 h5">Pilih Mapel</h2>
          </div>
          <div class="card-body">
            <form action="{{ route('storeJadwal', [$kelas->id, $hari->id]) }}" method="POST">
              @csrf
              <div class="form-group mb-4">
                <label for="states_0">Daftar Mapel</label>
                <select id="states_0" class="w-100" name="mapel_id">
                  @foreach ($mapel_s as $mapel)
                  <option hidden value="{{ $mapel->id }}">{{ $mapel->nama_mapel}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group mb-4">
                <div class="row">
                  <div class="col-md-6">
                    <label for="time_mulai">Mulai Pelajaran</label>
                    <div class="input-group">
                      <span class="input-group-text" id="basic-addon3"><span class="fas fa-time"></span></span>
                      <input type="time" class="form-control" placeholder="" id="time_mulai" name="jam_mulai">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="time_akhir">Akhir Pelajaran</label>
                    <div class="input-group">
                      <span class="input-group-text" id="basic-addon3"><span class="fas fa-time"></span></span>
                      <input type="time" class="form-control" placeholder="" id="time_akhir" name="jam_akhir">
                    </div>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-block btn-primary">Insert</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="table-settings my-5">
  <div class="row align-items-center justify-content-between">
    <div class="col col-md-6 col-lg-3 col-xl-4">
      <div class="btn-toolbar dropdown">
        <button data-toggle="modal" data-target="#modal-form-signup" class="btn btn-primary btn-sm mr-2"><span class="fas fa-plus mr-2"></span>New Task</button>
      </div>
    </div>
  </div>
</div>

<div class="row">
@foreach ($jadwal_s as $jadwal)

{{-- modal update mapel --}}
<div class="modal fade" id="modal-update-mapel{{$loop->iteration}}" tabindex="-1" role="dialog" aria-labelledby="modal-update-mapel{{$loop->iteration}}" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body p-0">
        <div class="card border-light p-4">
          <div class="card-header text-center pb-0">
            <h2 class="mb-0 h5">Pilih Mapel</h2>
          </div>
          <div class="card-body">
            <form action="{{ route('updateJadwal', [$kelas->id, $hari->id, $jadwal->id]) }}" method="POST">
              @csrf
              <div class="form-group mb-4">
                <label for="mapel_{{$loop->iteration}}">Daftar Mapel</label>
                <select id="mapel_{{$loop->iteration}}" class="w-100" name="mapel_id">
                  @foreach ($mapel_s as $mapel_update)
                  <option value="{{ $mapel_update->id }}" @if($jadwal->mapel_id === $mapel_update->id) selected @endif>{{ $mapel_update->nama_mapel}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group mb-4">
                <label for="tahun_{{$loop->iteration}}">Tahun Pelajaran</label>
                <select id="tahun_{{$loop->iteration}}" class="w-100" name="tahun_id">
                  @foreach ($tahun_s as $tahun)
                  <option value="{{ $tahun->id }}" @if($jadwal->tahun_id === $tahun->id) selected @endif>{{ $tahun->tahun }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group mb-4">
                <div class="row">
                  <div class="col-md-6">
                    <label for="time_mulai">Mulai Pelajaran</label>
                    <div class="input-group">
                      <span class="input-group-text" id="basic-addon3"><span class="fas fa-time"></span></span>
                      <input type="time" class="form-control" placeholder="" id="time_mulai" name="jam_mulai" value="{{ $jadwal->jam_mulai }}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="time_akhir">Akhir Pelajaran</label>
                    <div class="input-group">
                      <span class="input-group-text" id="basic-addon3"><span class="fas fa-time"></span></span>
                      <input type="time" class="form-control" placeholder="" id="time_akhir" name="jam_akhir" value="{{ $jadwal->jam_akhir }}">
                    </div>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-block btn-primary">Insert</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  {{-- modal input guru --}}
  <div class="modal fade" id="modal-guru{{$loop->iteration}}" tabindex="-1" role="dialog" aria-labelledby="modal-guru{{$loop->iteration}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body p-0">
          <div class="card border-light p-4">
            <div class="card-header text-center pb-0">
              <h2 class="mb-0 h5">Pilih Guru</h2>
            </div>
            <div class="card-body">
              <form action="{{ route('settingGuru', [$kelas->id, $hari->id, $jadwal->id]) }}" method="POST">
                @csrf
                <div class="form-group mb-4">
                  <label for="guru_{{$loop->iteration}}">Daftar Guru</label>
                  <select id="guru_{{$loop->iteration}}" class="w-100" name="user_id">
                    @foreach ($jadwal->mapel->guru_s as $guru_mapel)
                    <option hidden value="{{ $guru_mapel->id }}">{{ $guru_mapel->name}}</option>
                    @endforeach
                  </select>
                </div>
                <button type="submit" class="btn btn-block btn-primary">Insert</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 

  <div class="col-md-6 my-2">
    <div class="card hover-state border-bottom rounded-2">
      <div class="card-body d-sm-flex align-items-center flex-wrap flex-lg-nowrap py-2">
        <div class="col-10 col-lg-8 d-block px-0">
          <h3 class="h5">{{ $jadwal->mapel->nama_mapel }}</h3>
          <div class="d-block d-sm-flex">
            <div>
              <h4 class="h6 font-weight-normal text-gray mb-3 mb-sm-0"><span class="fas fa-clock mr-2"></span>{{ date('h:i A', strtotime($jadwal->jam_mulai)) }} - {{ date('h:i A', strtotime($jadwal->jam_akhir)) }}</h4>
            </div>
          </div>
          <div class="mt-3 pb-1">
            @if($jadwal->user_id !== Null)
            <button data-toggle="modal" data-target="#modal-guru{{$loop->iteration}}" class="btn btn-primary btn-sm mr-2"><span class="fas fa-chalkboard-teacher mr-2"></span>{{ $jadwal->user->name }}</button>
            @else
            <button data-toggle="modal" data-target="#modal-guru{{$loop->iteration}}" class="btn btn-primary btn-sm mr-2"><span class="fas fa-chalkboard-teacher mr-2"></span>Insert Guru</button>
            @endif
          </div>
        </div>
        <div class="col-10 col-sm-2 col-lg-2 col-xl-2 d-none d-lg-block d-xl-inline-flex align-items-center ml-lg-auto text-right justify-content-end px-md-0">
          <div class="btn-group ml-md-3">
            <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <button data-toggle="modal" data-target="#modal-update-mapel{{$loop->iteration}}" class="dropdown-item"><span class="fas fa-edit mr-2"></span> Edit</button>
              <a class="dropdown-item text-danger" href="#"><span class="fas fa-trash-alt mr-2"></span>Delete</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  
@endforeach
</div>


{{-- konten --}}
@endsection