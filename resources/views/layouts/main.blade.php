<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>App_school</title>
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <meta name="title" content="App_school">
  <meta name="author" content="">
  <meta name="description" content="App_school">
  <meta name="keywords" content="bootstrap 5, bootstrap, bootstrap 5 admin dashboard, bootstrap 5 dashboard, bootstrap 5 charts, bootstrap 5 calendar, bootstrap 5 datepicker, bootstrap 5 tables, bootstrap 5 datatable, vanilla js datatable, ,  dashboard,  admin dashboard">
  <link rel="canonical" href="https://appschool.com">
  <meta property="og:type" content="website">
  <meta property="og:url" content="https://appschool.com">
  <meta property="og:title" content="App_school">
  <meta property="og:description" content="App_school">
  <meta property="og:image" content="https://appschool.com">
  <meta property="twitter:card" content="summary_large_image">
  <meta property="twitter:url" content="https://appschool.com">
  <meta property="twitter:title" content="App_school">
  <meta property="twitter:description" content="App_school">
  <meta property="twitter:image" content="https://appschool.com">

  <link rel="apple-touch-icon" sizes="120x120" href="{{ url('backend/assets/img/favicon/apple-touch-icon.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ url('backend/assets/img/favicon/favicon-32x32.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ url('backend/assets/img/favicon/favicon-16x16.png') }}">
  <link rel="manifest" href="{{ url('backend/assets/img/favicon/site.webmanifest') }}">
  <link rel="mask-icon" href="{{ url('backend/assets/img/favicon/safari-pinned-tab.svg') }}" color="#ffffff">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="theme-color" content="#ffffff">
  <link type="text/css" href="{{ url('backend/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ url('backend/vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ url('backend/vendor/notyf/notyf.min.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ url('backend/vendor/fullcalendar/main.min.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ url('backend/vendor/dropzone/dist/min/dropzone.min.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ url('backend/vendor/choices.js/public/assets/styles/choices.min.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ url('backend/vendor/leaflet/dist/leaflet.css') }}" rel="stylesheet">
  <link type="text/css" href="{{ url('backend/css/volt.css') }}" rel="stylesheet">
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141734189-6"></script>
  <script>window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());
    gtag('config', 'UA-141734189-6');</script>
</head>

<body>
  <nav class="navbar navbar-dark navbar-theme-primary px-4 col-12 d-md-none">
    <a class="navbar-brand mr-lg-5" href="">
      <img class="navbar-brand-dark" src="{{ url('backend/assets/img/brand/light.svg') }}" alt="Volt logo">
      <img class="navbar-brand-light" src="{{ url('backend/assets/img/brand/dark.svg') }}" alt="Volt logo">
    </a>
    <div class="d-flex align-items-center">
      <button class="navbar-toggler d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
    </div>
  </nav>

  <div class="container-fluid bg-soft">
    <div class="row">
      <div class="col-12">
        <nav id="sidebarMenu" class="sidebar d-md-block bg-primary text-white collapse" data-simplebar>
          <div class="sidebar-inner px-4 pt-3">
            <ul class="nav flex-column">
              <li class="nav-item">
                <span class="nav-link d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#submenu-dashboard"><span>
                  <span class="sidebar-icon"><span class="fas fa-chart-pie"></span></span> Dashboard </span>
                  <span class="link-arrow"><span class="fas fa-chevron-right"></span></span>
                </span>
                <div class="multi-level collapse show" role="list" id="submenu-dashboard" aria-expanded="false">
                  <ul class="flex-column nav">
                    <li class="nav-item active"><a href="{{ route('dashboard') }}" class="nav-link"><span>Overview</span></a></li>
                    <li class="nav-item"><a href="{{ route('profile') }}" class="nav-link"><span>Profile</span></a></li>
                  </ul>
                </div>
              </li>

              @if(session('role') === 'Operator')
              <li class="nav-item"><a href="{{ route('indexTahunAjar') }}" class="nav-link"><span class="sidebar-icon"><span class="fas fa-vote-yea"></span></span><span>Tahun Ajar</span></a></li>
              <li class="nav-item"><a href="{{ route('indexGuru') }}" class="nav-link"><span class="sidebar-icon"><span class="fas fa-chalkboard-teacher"></span></span><span>List Guru</span></a></li>
              <li class="nav-item"><a href="{{ route('indexSiswa') }}" class="nav-link"><span class="sidebar-icon"><span class="fas fa-user-check"></span></span><span>List Siswa</span></a></li>
              <li class="nav-item"><a href="{{ route('indexKelas') }}" class="nav-link"><span class="sidebar-icon"><span class="fas fa-inbox"></span></span><span>List Kelas</span></a></li>
              <li class="nav-item"><a href="{{ route('indexMapel') }}" class="nav-link"><span class="sidebar-icon"><span class="fas fa-th-list"></span></span><span>List Mapel</span></a></li>
              @elseif(session('role') === 'Guru' || session('role') === 'Siswa')
              <li class="nav-item"><a href="{{ route('indexClassroom') }}" class="nav-link"><span class="sidebar-icon"><span class="fas fa-chalkboard"></span></span><span>Classroom</span></a></li>
              @endif
              <li role="separator" class="dropdown-divider mt-4 mb-3 border-black"></li>
              <li class="nav-item"><a href="{{ route('indexPengumuman') }}" target="_blank" class="nav-link d-flex align-items-center"><span class="sidebar-icon"><span class="fas fa-volume-up"></span></span> <span>Pengumuman</span></a></li>
            </ul>
          </div>
        </nav>

        <main class="content">
          <nav class="navbar navbar-top navbar-expand navbar-dashboard navbar-dark pl-0 pr-2 pb-0">
            <div class="container-fluid px-0">
              <div class="d-flex justify-content-between w-100" id="navbarSupportedContent">
                <div class="d-flex">
                  <form class="navbar-search form-inline" id="navbar-search-main">
                    <div class="input-group input-group-merge search-bar">
                      <span class="input-group-text" id="topbar-addon"><span class="fas fa-search"></span></span>
                      <input type="text" class="form-control" id="topbarInputIconLeft" placeholder="Search" aria-label="Search" aria-describedby="topbar-addon">
                    </div>
                  </form>
                </div>
                <div class="d-flex">
                  <ul class="navbar-nav align-items-center">
                    <li class="nav-item dropdown">
                      <a class="nav-link text-dark mt-2 mr-lg-3 icon-notifications" data-unread-notifications="true" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="icon icon-sm"><span class="fas fa-bell bell-shake"></span><span class="icon-badge rounded-circle unread-notifications"></span></span>
                      </a>
                      <div class="dropdown-menu dashboard-dropdown dropdown-menu-lg dropdown-menu-right mt-2 py-0">
                        <div class="list-group list-group-flush">
                          <a href="{{ route('indexPengumuman') }}" class="text-center text-primary font-weight-bold border-bottom border-light py-3">Pengumuman</a>
                          <a href="" class="list-group-item list-group-item-action border-bottom border-light">
                            <div class="row align-items-center">
                              <div class="col-auto"><span class="fas fa-volume-up"></span></div>
                              <div class="col pl-0 ml--2">
                                <p class="font-small mt-1 mb-0">Kerja Bakti Setelah Liburan</p>
                                <div class="d-flex justify-content-between align-items-center">
                                  <div class="text-right"><small class="text-danger">a few moments ago</small></div>
                                </div>
                              </div>
                            </div>
                          </a>
                          <a href="#" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>
                        </div>
                      </div>
                    </li>

                    <li class="nav-item dropdown pl-2">
                      <a class="nav-link pt-1 px-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media d-flex align-items-center">
                          <img class="user-avatar md-avatar rounded-circle" alt="Image placeholder" src="{{ url('backend/assets/img/team/profile-picture-3.jpg') }}">
                          <div class="media-body ml-2 text-dark align-items-center d-none d-lg-block">
                            <span class="mb-0 font-small font-weight-bold">{{ Auth::user()->name }}</span>
                          </div>
                        </div>
                      </a>
                      <div class="dropdown-menu dashboard-dropdown dropdown-menu-right mt-2">
                        <a class="dropdown-item font-weight-bold" href="{{ route('profile') }}"><span class="far fa-user-circle"></span>My Profile</a>
                        <a class="dropdown-item font-weight-bold" href="{{ route('dashboard') }}"><span class="fas fa-cog"></span>Overview</a>
                        <div role="separator" class="dropdown-divider"></div>
                        <form method="POST" action="{{ route('logout') }}">
                          @csrf <button type="submit" class="dropdown-item font-weight-bold" href="#"><span class="fas fa-sign-out-alt text-danger"></span>{{ __('Logout') }}</button>
                        </form>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
          <div class="preloader bg-soft flex-column justify-content-center align-items-center"><img
              class="loader-element animate__animated animate__jackInTheBox" src="{{ url('backend/assets/img/brand/light.svg') }}"
              height="60" alt="Volt logo">
          </div>

          @yield('content')

          <footer class="footer section py-5">
            <div class="row">
              <div class="col-12 col-lg-6 mb-4 mb-lg-0">
                <p class="mb-0 text-center text-xl-left">Copyright © - <span class="current-year"></span></p>
              </div>
              <div class="col-12 col-lg-6">
                <ul class="list-inline list-group-flush list-group-borderless text-center text-xl-right mb-0">
                  <li class="list-inline-item px-0 px-sm-2"><a href="https://.com/about">Pengumuman</a></li>
                </ul>
              </div>
            </div>
          </footer>
        </main>
      </div>
    </div>
  </div>

  <script src="{{ url('backend/vendor/popper.js/dist/umd/popper.min.js') }}"></script>
  <script src="{{ url('backend/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ url('backend/vendor/onscreen/dist/on-screen.umd.min.js') }}"></script>
  <script src="{{ url('backend/vendor/nouislider/distribute/nouislider.min.js') }}"></script>
  <script src="{{ url('backend/vendor/jarallax/dist/jarallax.min.js') }}"></script>
  <script src="{{ url('backend/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') }}"></script>
  <script src="{{ url('backend/vendor/countup.js/dist/countUp.umd.js') }}"></script>
  <script src="{{ url('backend/vendor/chartist/dist/chartist.min.js') }}"></script>
  <script src="{{ url('backend/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}"></script>
  <script src="{{ url('backend/vendor/vanillajs-datepicker/dist/js/datepicker.min.js') }}"></script>
  <script src="{{ url('backend/vendor/simple-datatables/dist/umd/simple-datatables.js') }}"></script>
  <script src="{{ url('backend/vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js') }}"></script>
  <script src="{{ url('backend/vendor/vanillajs-datepicker/dist/js/datepicker.min.js') }}"></script>
  <script src="{{ url('backend/vendor/fullcalendar/main.min.js') }}"></script>
  <script src="{{ url('backend/vendor/dropzone/dist/min/dropzone.min.js') }}"></script>
  <script src="{{ url('backend/vendor/choices.js/public/assets/scripts/choices.min.js') }}"></script>
  <script src="{{ url('backend/vendor/notyf/notyf.min.js') }}"></script>
  <script src="{{ url('backend/vendor/leaflet/dist/leaflet.js') }}"></script>
  <script src="{{ url('backend/vendor/svgmap/dist/svgMap.min.js') }}"></script>
  <script src="{{ url('backend/vendor/simplebar/dist/simplebar.min.js') }}"></script>
  <script async defer="defer" src="https://buttons.github.io/buttons.js') }}"></script>
  <script src="{{ url('backend/assets/js/volt.js') }}"></script>
</body>

</html>