<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/dashboard', function () {
    return view('backend/dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/profile', 'Backend\UserController@index')->name('profile');
// route tahun ajar
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/tahun-ajar', 'Backend\TahunController@index')->name('indexTahunAjar');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/tahun-ajar/store', 'Backend\TahunController@store')->name('storeTahunAjar');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/tahun-ajar/{tahun_id}/update', 'Backend\TahunController@update')->name('updateTahunAjar');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/tahun-ajar/{tahun_id}/set-active', 'Backend\TahunController@active')->name('activeTahunAjar');


Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/guru', 'Backend\GuruController@index')->name('indexGuru');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/guru/create', 'Backend\GuruController@create')->name('createGuru');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/guru/store', 'Backend\GuruController@store')->name('storeGuru');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/guru/{id}/edit', 'Backend\GuruController@edit')->name('editGuru');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/guru/{id}/update', 'Backend\GuruController@update')->name('updateGuru');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/guru/{id}/inactive', 'Backend\GuruController@inactive')->name('inactiveGuru');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/guru/{id}/active', 'Backend\GuruController@active')->name('activeGuru');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/guru/{id}/reset-password', 'Backend\GuruController@reset')->name('resetGuru');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/siswa', 'Backend\SiswaController@index')->name('indexSiswa');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/kelas', 'Backend\KelasController@index')->name('indexKelas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/kelas/create', 'Backend\KelasController@create')->name('createKelas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/kelas/store', 'Backend\KelasController@store')->name('storeKelas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/kelas/{id}/edit', 'Backend\KelasController@edit')->name('editKelas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/kelas/{id}/update', 'Backend\KelasController@update')->name('updateKelas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/kelas/{id}/show', 'Backend\KelasController@show')->name('showKelas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/kelas/{id}/create-siswa', 'Backend\KelasController@createSiswa')->name('createSiswaKelas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/kelas/{id}/create-siswa', 'Backend\KelasController@storeSiswa')->name('storeSiswaKelas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/kelas/{id}/hari', 'Backend\KelasController@indexHariKelas')->name('hariKelas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/kelas/{kelas_id}/hari/{hari_id}', 'Backend\KelasController@showHariKelas')->name('showHariKelas');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/kelas/{kelas_id}/hari/{hari_id}/insert-mapel', 'Backend\JadwalController@store')->name('storeJadwal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/kelas/{kelas_id}/hari/{hari_id}/jadwal/{jabwal_id}/update-mapel', 'Backend\JadwalController@update')->name('updateJadwal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/kelas/{kelas_id}/hari/{hari_id}/jadwal/{jadwal_id}/insert-guru', 'Backend\JadwalController@insertGuru')->name('settingGuru');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/mapel', 'Backend\MapelController@index')->name('indexMapel');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/mapel/create', 'Backend\MapelController@create')->name('createMapel');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/mapel/store', 'Backend\MapelController@store')->name('storeMapel');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/mapel/{id}/edit', 'Backend\MapelController@edit')->name('editMapel');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/mapel/{id}/update', 'Backend\MapelController@update')->name('updateMapel');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/mapel/{id}/show', 'Backend\MapelController@show')->name('showMapel');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/mapel/{mapel_id}/insert-guru', 'Backend\MapelController@insert')->name('insertGuruMapel');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/mapel/{mapel_id}/guru/{guru_id}/out', 'Backend\MapelController@out')->name('outGuruMapel');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/pengumuman', 'Backend\PengumumanController@index')->name('indexPengumuman');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/pengumuman/create', 'Backend\PengumumanController@create')->name('createPengumuman');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/pengumuman/store', 'Backend\PengumumanController@store')->name('storePengumuman');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/pengumuman/{pengumuman_id}/edit', 'Backend\PengumumanController@edit')->name('editPengumuman');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/pengumuman/{pengumuman_id}/update', 'Backend\PengumumanController@update')->name('updatePengumuman');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/pengumuman/{pengumuman_id}/destroy', 'Backend\PengumumanController@destroy')->name('destroyPengumuman');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/pengumuman/{pengumuman_id}/show', 'Backend\PengumumanController@show')->name('showPengumuman');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/blog', 'Backend\BlogController@index')->name('indexBlog');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom', 'Backend\ClassroomController@index')->name('indexClassroom');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/show', 'Backend\ClassroomController@show')->name('showClassroom');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}', 'Backend\MateriController@index')->name('indexMateri');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/create', 'Backend\MateriController@create')->name('createMateri');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/store', 'Backend\MateriController@store')->name('storeMateri');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/show', 'Backend\MateriController@show')->name('showMateri');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/edit', 'Backend\MateriController@edit')->name('editMateri');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/update', 'Backend\MateriController@update')->name('updateMateri');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/destroy', 'Backend\MateriController@destroy')->name('destroyMateri');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/ebook/store', 'Backend\EbookController@store')->name('storeEbook');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/ebook/{ebook_id}/update', 'Backend\EbookController@update')->name('updateEbook');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/ebook/{ebook_id}/destroy', 'Backend\EbookController@destroy')->name('destroyEbook');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/ebook/{ebook_id}/download', 'Backend\EbookController@download')->name('downloadEbook');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/store', 'Backend\TugasController@store')->name('storeTugas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/{tugas_id}/update', 'Backend\TugasController@update')->name('updateTugas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/{tugas_id}/', 'Backend\TugasController@index')->name('indexTugas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/{tugas_id}/show', 'Backend\TugasController@show')->name('showTugas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/{tugas_id}/destroy', 'Backend\TugasController@destroy')->name('destroyTugas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/{tugas_id}/download', 'Backend\TugasController@download')->name('downloadTugas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/{tugas_id}/upload', 'Backend\TugasController@upload')->name('uploadTugas');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/{tugas_id}/siswa/{siswa_id}/download', 'Backend\TugasController@downloadTugasSiswa')->name('downloadTugasSiswa');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/{tugas_id}/siswa/{siswa_id}/destroy', 'Backend\TugasController@destroyTugasSiswa')->name('destroyTugasSiswa');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/tugas/{tugas_id}/siswa/{siswa_id}/nilai', 'Backend\TugasController@nilaiTugasSiswa')->name('nilaiTugasSiswa');

Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/store', 'Backend\SoalController@store')->name('storeSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/update', 'Backend\SoalController@update')->name('updateSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/destroy', 'Backend\SoalController@destroy')->name('destroySoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/show', 'Backend\SoalController@show')->name('showSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/setting', 'Backend\SettingSoalController@index')->name('indexSettingSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/kunci-jawaban', 'Backend\SettingSoalController@createKunciSoal')->name('createKunciSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/kunci-jawaban', 'Backend\SettingSoalController@storeKunciSoal')->name('storeKunciSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/kerjakan', 'Backend\SettingSoalController@kerjakanSoal')->name('kerjakanSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/kerjakan', 'Backend\SettingSoalController@storeKerjakanSoal')->name('storeKerjakanSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/segment/store', 'Backend\SettingSoalController@storeSegmentSoal')->name('storeSegmentSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/segment/{segment_id}/update', 'Backend\SettingSoalController@updateSegmentSoal')->name('updateSegmentSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/segment/{segment_id}/destroy', 'Backend\SettingSoalController@destroySegmentSoal')->name('destroySegmentSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/segment/{segment_id}/copot', 'Backend\SettingSoalController@copotSegmentSoal')->name('copotSegmentSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/segment/{segment_id}/pasang', 'Backend\SettingSoalController@pasangSegmentSoal')->name('pasangSegmentSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/segment/{segment_id}', 'Backend\SettingSoalController@indexSegmentSoal')->name('indexSegmentSoal');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/segment/{segment_id}/pertanyaan/store', 'Backend\SettingSoalController@storePertanyaan')->name('storePertanyaan');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->post('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/segment/{segment_id}/pertanyaan/{pertanyaan_id}/update', 'Backend\SettingSoalController@updatePertanyaan')->name('updatePertanyaan');
Route::middleware(['auth:sanctum', 'verified', 'rolemiddleware'])->get('/classroom-hari/{slug_hari}/kelas/{kelas_id}/mapel/{mapel_id}/materi/{materi_id}/soal/{soal_id}/segment/{segment_id}/pertanyaan/{pertanyaan_id}/destroy', 'Backend\SettingSoalController@destroyPertanyaan')->name('destroyPertanyaan');



